let hours = document.querySelector('.hours');
let minutes = document.querySelector('.minutes');
let seconds = document.querySelector('.seconds');

let month = document.querySelector('.month');
let day = document.querySelector('.day');
let year = document.querySelector('.year');

function digitalDate() {
    let now = new Date();
    let mm = now.getMonth();
    let dd = now.getDate();
    let yyyy = now.getFullYear();
    let sec = now.getSeconds();
    let min = now.getMinutes();
    let hrs = now.getHours();
    let monthName = ['January', 'February', 'March', 'April',
        'May', 'June', 'July', 'August', 'September',
        'October', 'November', 'December'];

    if (hrs < 10) {
        hours.innerHTML = '0' + hrs;
    } else {
        hours.innerHTML = hrs;
    }

    if (min < 10) {
        minutes.innerHTML = '0' + min;
    } else {
        minutes.innerHTML = min;
    }

    if (sec < 10) {
        seconds.innerHTML = '0' + sec;
    } else {
        seconds.innerHTML = sec;
    }
    month.innerHTML = monthName[mm];
    day.innerHTML = dd;
    year.innerHTML = yyyy;
}

digitalDate();
setInterval(digitalDate, 1000);